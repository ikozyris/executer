/*
 * Copyright (C) 2023  ikozyris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * powersaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Popups 1.3

import Executer 1.0


MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'excuter.ikozyris'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent
        width: parent.width

        header: PageHeader {
            id: header
            title: i18n.tr("Executer, run any command")
            ActionBar {
                anchors {
                    top: parent.top
                    right: parent.right
                }
                numberOfSlots: 1
                actions: [
                    Action {
                        iconName: "info"
                        text: i18n.tr("About")
                        onTriggered: PopupUtils.open(aboutdialog);
                    }
                ]
            }
        }

        ColumnLayout {
            anchors.top: parent.header.bottom
            width: parent.width

            Launcher {
                id: qprocess
            }

            TextField {
                id: textField
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: parent.width
                horizontalAlignment: Text.AlignHCenter
                placeholderText: i18n.tr("Enter command here")
            }


            Button {
                id: button
                text: i18n.tr("Run!")
                Layout.alignment: Qt.AlignCenter
                onClicked: PopupUtils.open(dialog)
            }

            Component {
                 id: dialog
                 Dialog {
                     id: dialogue
                     title: (textField.text)
                     property var output : qprocess.launch(textField.text);
                     text: (output)
                     Button {
                         text: i18n.tr("OK")
                         color: "gray"
                         onClicked: PopupUtils.close(dialogue)
                     }
                     Button {
                         text: i18n.tr("Copy command to clipboard")
                         color: "gray"
                         onClicked: Clipboard.push(textField.text);
                     }
                     Button {
                         text: i18n.tr("Copy output to clipboard")
                         color: "gray"
                         onClicked: Clipboard.push(output);
                     }
                 }
            }

            Component {
                id: aboutdialog
                Dialog {
                    id: aboutdialogue
                    Label {
                        width: parent.width
                        wrapMode: Text.WordWrap
                        text: i18n.tr("This app was developed by ikozyris. It is a simple unconfied app that runs the commands you inputed. Warning: Interactive apps will not work e.g. sudo. Licensed under GNU GPL v3. Source code on: https://gitlab.com/ikozyris/executer")
                    }
                    Button {
                        text: i18n.tr("Close")
                        onClicked: PopupUtils.close(aboutdialogue)
                    }
                }
            }

            Item {
                Layout.fillHeight: true
            }
        }
    }
}
